Layout
======

Provides reusable layout features for own content elements.

## Usage

When creating your content elements, use one of these palettes:

* `vitd_layout`
* `vitd_layout_noanimation`

In your `Configuration/TCA/Overrides/tt_content.php`:

```php
$GLOBALS['TCA']['tt_content]['types']['yourext_yourcontentelement'] = [
    'showitem' => '
            foo;Foo,
            ...
        --div--;Layout,
            --palette--;Layout options;vitd_layout,
            ....
    ',
];
```

Later, when doing the content element templates ([Fluid](https://github.com/TYPO3/Fluid)), let
the included ViewHelper do it's magic:

```html
<html
	xmlns:layout="http://typo3.org/ns/VITD/Layout/ViewHelpers"
	xmlns:f="http://typo3.org/ns/TYPO3/Fluid/ViewHelpers">
<head>
	<title>Demo template</title>
	<f:layout name="Default" />
</head>
<body>	
	
<f:section name="Main">
	<layout:section row="{data}">
		<h1>{data.title}</h1>
		...
	</layout:section>
</f:section>

</body>
</html>
```

which results in

```html
<section id="c123" data-parallax="1.23" data-whatever-is-needed="editors choice">
	<div class="bg-overlay"></div>
	<div class="container">
		<h1>Content element title</h1>
		...
	</div>
</section>
```

Note: All of the `tt_content` fields are excludeable. Depending on your access control setup
(`$GLOBALS['TYPO3_CONF_VARS']['BE']['explicitADmode']`) you either **must whitelist** the fields your editors are
allowed to use or in case of `explicitDeny` mode you *can* blacklist those you don't want them to use.

## Features

* Animation
* Boxing (limit width)
* Background (texture, color, image, style)
* Padding configuration
* Parallax effect
* Responsive visibility (show/hide separately on desktop/tablet/mobile)
