<?php
$EM_CONF['layout'] = [
    'title' => 'Content element layout',
    'description' => 'Provides reusable layout features for own content elements',
    'category' => 'misc',
    'author' => 'Ludwig Rafelsberger',
    'author_email' => 'ludwig.rafelsberger@vitd.at',
    'author_company' => 'VITAMIN D GmbH',
    'state' => 'stable',
    'version' => '104.0.0',
    'constraints' => [
        'depends' => [
            'core' => '10.4.0-10.4.99',
            'fluid_styled_content' => '10.4.0-10.4.99',
            'frontend' => '10.4.0-10.4.99',
        ],
    ],
];
