<?php

namespace VITD\Layout;

/**
 * A html tag
 */
class Tag
{
    /** HTML tags known to be always self-closing */
    const SELF_CLOSING_TAGS = [
        'area', 'base', 'br', 'col', 'command', 'embed', 'hr', 'img', 'input', 'keygen', 'link', 'meta', 'param',
        'source', 'track', 'wbr'
    ];

    /**
     * Tag name
     *
     * @var string
     */
    protected $tagname;

    /**
     * Id
     *
     * @var string
     */
    protected $id;

    /**
     * Classes
     *
     * @var array
     */
    protected $classes = [];

    /**
     * Inline css styles
     *
     * @var array
     */
    protected $styles = [];

    /**
     * Data attributes
     *
     * @var array
     */
    protected $dataAttributes = [];




    // ---------------------- object lifecycle methods ----------------------
    /**
     * Create a new Tag
     *
     * @param string $tagname Name of the tag ("div", "section", …)
     */
    public function __construct(string $tagname)
    {
        $tagname = strtolower($tagname);
        $this->tagname = $tagname;
    }



    // ----------------------------- oo basics ------------------------------
    /**
     * Return the opening tag
     *
     * @return string Opening tag (might be self-closing)
     */
    public function __toString() :string
    {
        return sprintf(
            '<%s%s%s%s%s%s>',
            $this->tagname,
            (\is_string($this->id) && $this->id !== '' ? ' id="' . $this->id . '"' : ''),
            (!empty($this->classes) ? ' class="' . implode(' ', $this->classes) . '"' : ''),
            (\in_array($this->tagname, self::SELF_CLOSING_TAGS, true) ? '/' : ''),
            (
            !empty($this->dataAttributes)
                ? ' ' . implode(
                    ' ',
                    array_map(
                        function(string $a, string $v) { return sprintf('data-%s="%s"', $a, $v); },
                        array_keys($this->dataAttributes),
                        $this->dataAttributes
                    )
                )
                : ''
            ),
            (
            !empty($this->styles)
                ? ' style="'
                . implode(
                    ' ',
                    array_map(
                        function(string $p, string $v) { return sprintf('%s: %s;', $p, $v); },
                        array_keys($this->styles),
                        $this->styles
                    )
                )
                . '"'
                : ''
            )
        );
    }


    // -------------------------- classic methods ---------------------------
    /**
     * Get closing tag
     *
     * Empty string if the tag is self-closing and thus no extra closing tag is desired
     *
     * @return string Closing tag
     */
    public function getClosingTag() :string
    {
        return \in_array($this->tagname, self::SELF_CLOSING_TAGS, true)
            ? ''
            : sprintf('</%s>', $this->tagname);
    }


    // ----------------------------- accessors ------------------------------
    /**
     * Set the id attribute
     *
     * @param string $id Tag id (no sanity checks done: make sure this is valid!)
     *
     * @return Tag $this for fluent calls
     */
    public function setId(string $id) :Tag
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Add a class to the class list
     *
     * If a class is already in the list, this method does nothing.
     * If the given class is empty, this method does nothing.
     *
     * @param string $className Class name to add
     *
     * @return Tag $this for fluent calls
     */
    public function addClass(string $className) :Tag
    {
        if ($className !== '' && !\in_array($className, $this->classes, true)) {
            $this->classes[] = $className;
        }
        return $this;
    }

    /**
     * Set a data attribute
     *
     * If a data attribute with the same name already exists it will be replaced by the new value
     *
     * @param string $attribute Data attribute name
     * @param string $value Data value
     *
     * @return Tag $this for fluent calls
     */
    public function setData(string $attribute, string $value) :Tag
    {
        $this->dataAttributes[$attribute] = $value;
        return $this;
    }

    /**
     * Set a css property
     *
     * If the same property is already declared it will be replaced by the new value
     *
     * @param string $property CSS property
     * @param string $value New property value
     *
     * @return Tag $this for fluent calls
     */
    public function setStyle(string $property, string $value) :Tag
    {
        $this->styles[$property] = $value;
        return $this;
    }
}
