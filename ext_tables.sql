--
-- Enhancements to tt_content table
--
create table tt_content (
	animation enum('', 'bounce', 'flash', 'pulse', 'rubberBand', 'shake', 'swing', 'tada', 'wobble', 'jello', 'bounceIn', 'bounceInDown', 'bounceInLeft', 'bounceInRight', 'bounceInUp', 'bounceOut', 'bounceOutDown', 'bounceOutLeft', 'bounceOutRight', 'bounceOutUp', 'fadeIn', 'fadeInDown', 'fadeInDownBig', 'fadeInLeft', 'fadeInLeftBig', 'fadeInRight', 'fadeInRightBig', 'fadeInUp', 'fadeInUpBig', 'fadeOut', 'fadeOutDown', 'fadeOutDownBig', 'fadeOutLeft', 'fadeOutLeftBig', 'fadeOutRight', 'fadeOutRightBig', 'fadeOutUp', 'fadeOutUpBig', 'flipInX', 'flipInY', 'flipOutX', 'flipOutY', 'LightSpeedIn', 'LightSpeedOut', 'rotateIn', 'rotateInDownLeft', 'rotateInDownRight', 'rotateInUpLeft', 'rotateInUpRight', 'rotateOut', 'rotateOutDownLeft', 'rotateOutDownRight', 'rotateOutUpLeft', 'rotateOutUpRight', 'slideInUp', 'slideInDown', 'slideInLeft', 'slideInRight', 'slideOutUp', 'slideOutDown', 'slideOutLeft', 'slideOutRight', 'zoomIn', 'zoomInUp', 'zoomInDown', 'zoomInLeft', 'zoomInRight', 'zoomOut', 'zoomOutUp', 'zoomOutDown', 'zoomOutLeft', 'zoomOutRight', 'hinge', 'jackInTheBox', 'rollIn', 'rollOut') default '',
	boxedbackground smallint unsigned not null default 0,
	boxedcontent smallint unsigned not null default 1,
	hintergrundbild int unsigned not null default 0,
	hintergrundfarbe varchar(32) default '' not null,
	hintergrundmuster tinyint unsigned not null default 0,
	hintergrundstil tinyint unsigned not null default 0,
	paddingbottom int not null default 60,
	paddingtop int not null default 60,
	parallax varchar(4) not null default '1.00',
	responsive_visibility tinyint unsigned not null default 7
);
