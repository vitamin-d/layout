<?php
defined('TYPO3_MODE') or die();

(function (string $table) {

    // ------------------------- additional columns -------------------------
    $GLOBALS['TCA'][$table]['columns']['animation'] = [
        'exclude' => true,
        'label' => 'Animation',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                ['Keine', ''],
                ['Attention Seekers', '--div--'],
                ['bounce', 'bounce'],
                ['flash', 'flash'],
                ['pulse', 'pulse'],
                ['rubberBand', 'rubberBand'],
                ['shake', 'shake'],
                ['swing', 'swing'],
                ['tada', 'tada'],
                ['wobble', 'wobble'],
                ['jello', 'jello'],
                ['Bouncing Entrances', '--div--'],
                ['bounceIn', 'bounceIn'],
                ['bounceInDown', 'bounceInDown'],
                ['bounceInLeft', 'bounceInLeft'],
                ['bounceInRight', 'bounceInRight'],
                ['bounceInUp', 'bounceInUp'],
                ['Bouncing Exits', '--div--'],
                ['bounceOut', 'bounceOut'],
                ['bounceOutDown', 'bounceOutDown'],
                ['bounceOutLeft', 'bounceOutLeft'],
                ['bounceOutRight', 'bounceOutRight'],
                ['bounceOutUp', 'bounceOutUp'],
                ['Fading Entrances', '--div--'],
                ['fadeIn', 'fadeIn'],
                ['fadeInDown', 'fadeInDown'],
                ['fadeInDownBig', 'fadeInDownBig'],
                ['fadeInLeft', 'fadeInLeft'],
                ['fadeInLeftBig', 'fadeInLeftBig'],
                ['fadeInRight', 'fadeInRight'],
                ['fadeInRightBig', 'fadeInRightBig'],
                ['fadeInUp', 'fadeInUp'],
                ['fadeInUpBig', 'fadeInUpBig'],
                ['Fading Exits', '--div--'],
                ['fadeOut', 'fadeOut'],
                ['fadeOutDown', 'fadeOutDown'],
                ['fadeOutDownBig', 'fadeOutDownBig'],
                ['fadeOutLeft', 'fadeOutLeft'],
                ['fadeOutLeftBig', 'fadeOutLeftBig'],
                ['fadeOutRight', 'fadeOutRight'],
                ['fadeOutRightBig', 'fadeOutRightBig'],
                ['fadeOutUp', 'fadeOutUp'],
                ['fadeOutUpBig', 'fadeOutUpBig'],
                ['Flippers', '--div--'],
                ['flip', 'flip'],
                ['flipInX', 'flipInX'],
                ['flipInY', 'flipInY'],
                ['flipOutX', 'flipOutX'],
                ['flipOutY', 'flipOutY'],
                ['Lightspeed', '--div--'],
                ['lightSpeedIn', 'LightSpeedIn'],
                ['lightSpeedOut', 'LightSpeedOut'],
                ['Rotating Entrances', '--div--'],
                ['rotateIn', 'rotateIn'],
                ['rotateInDownLeft', 'rotateInDownLeft'],
                ['rotateInDownRight', 'rotateInDownRight'],
                ['rotateInUpLeft', 'rotateInUpLeft'],
                ['rotateInUpRight', 'rotateInUpRight'],
                ['Rotating Exits', '--div--'],
                ['rotateOut', 'rotateOut'],
                ['rotateOutDownLeft', 'rotateOutDownLeft'],
                ['rotateOutDownRight', 'rotateOutDownRight'],
                ['rotateOutUpLeft', 'rotateOutUpLeft'],
                ['rotateOutUpRight', 'rotateOutUpRight'],
                ['Sliding Entrances', '--div--'],
                ['slideInUp', 'slideInUp'],
                ['slideInDown', 'slideInDown'],
                ['slideInLeft', 'slideInLeft'],
                ['slideInRight', 'slideInRight'],
                ['Sliding Exits', '--div--'],
                ['slideOutUp', 'slideOutUp'],
                ['slideOutDown', 'slideOutDown'],
                ['slideOutLeft', 'slideOutLeft'],
                ['slideOutRight', 'slideOutRight'],
                ['Zoom Entrances', '--div--'],
                ['zoomIn', 'zoomIn'],
                ['zoomInUp', 'zoomInUp'],
                ['zoomInDown', 'zoomInDown'],
                ['zoomInLeft', 'zoomInLeft'],
                ['zoomInRight', 'zoomInRight'],
                ['Zoom Exits', '--div--'],
                ['zoomOut', 'zoomOut'],
                ['zoomOutUp', 'zoomOutUp'],
                ['zoomOutDown', 'zoomOutDown'],
                ['zoomOutLeft', 'zoomOutLeft'],
                ['zoomOutRight', 'zoomOutRight'],
                ['Specials', '--div--'],
                ['hinge', 'hinge'],
                ['jackInTheBox', 'jackInTheBox'],
                ['rollIn', 'rollIn'],
                ['rollOut', 'rollOut'],
            ],
            'default' => '',
            'size' => 1,
        ],
    ];

    $GLOBALS['TCA'][$table]['columns']['boxedbackground'] = [
        'exclude' => true,
        'label' => 'Hintergrund mit fixer Breite anzeigen',
        'config' => [
            'type' => 'check',
            'default' => 0,
        ],
    ];

    $GLOBALS['TCA'][$table]['columns']['boxedcontent'] = [
        'exclude' => true,
        'label' => 'Inhalt mit fixer Breite anzeigen',
        'config' => [
            'type' => 'check',
            'default' => 1,
        ],
    ];

    $GLOBALS['TCA'][$table]['columns']['hintergrundbild'] = [
        'label' => 'Hintergrundbild',
        'exclude' => true,
        'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
            'hintergrundbild',
            [
                'appearance' => [
                    'createNewRelationLinkTitle' => 'LLL:EXT:cms/locallang_ttc.xlf:media.addFileReference',
                ],
                'maxitems' => 1,
            ]
        ),
    ];

    $GLOBALS['TCA'][$table]['columns']['hintergrundfarbe'] = [
        'exclude' => true,
        'label' => 'Hintergrundfarbe',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                ['Standard', ''],
                ['Weiß', 'white'],
            ],
            'default' => '',
            'size' => 1,
        ],
    ];

    $GLOBALS['TCA'][$table]['columns']['hintergrundmuster'] = [
        'exclude' => true,
        'label' => 'Hintergrundmuster anzeigen',
        'config' => [
            'type' => 'check',
            'default' => 0,
        ],
    ];

    $GLOBALS['TCA'][$table]['columns']['hintergrundstil'] = [
        'exclude' => true,
        'label' => 'Hintergrundstil',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                ['Hell', 0],
                ['Dunkel', 1],
            ],
            'default' => 0,
            'size' => 1,
        ],
    ];

    $GLOBALS['TCA'][$table]['columns']['paddingbottom'] = [
        'exclude' => true,
        'label' => 'Abstand unten (in px, Standard: 60px)',
        'config' => [
            'type' => 'input',
            'eval' => 'int',
            'size' => 4,
        ],
    ];

    $GLOBALS['TCA'][$table]['columns']['paddingtop'] = [
        'exclude' => true,
        'label' => 'Abstand oben (in px, Standard: 60px)',
        'config' => [
            'type' => 'input',
            'eval' => 'int',
            'size' => 4,
        ],
    ];

    $GLOBALS['TCA'][$table]['columns']['parallax'] = [
        'exclude' => true,
        'label' => 'Parallax-Effekt (1.00 ist deaktiviert, <1 ist langsamer, >1 ist schneller als Scrolling)',
        'config' => [
            'type' => 'input',
            'eval' => 'double2',
            'default' => '1.00',
        ],
    ];

    $GLOBALS['TCA'][$table]['columns']['responsive_visibility'] = [
        'exclude' => true,
        'label' => 'Größenabhängige Sichtbarkeit',
        'config' => [
            'type' => 'check',
            'items' => [
                ['Auf Desktopgeräten anzeigen', ''], // bit-value 1
                ['Auf Tablets anzeigen', ''], // bit-value 2
                ['Auf Mobilgeräten anzeigen', ''], // bit-value 4
            ],
            'default' => 7,
        ],
    ];


    // --------------------- provide reusable palettes ----------------------
    $GLOBALS['TCA'][$table]['palettes']['vitd_layout'] = [
        'showitem' => '
                boxedbackground, boxedcontent,
            --linebreak--,
                paddingtop, paddingbottom,
            --linebreak--,
                hintergrundfarbe, hintergrundbild, hintergrundmuster, hintergrundstil,
            --linebreak--,
                parallax,
            --linebreak--,
                animation,
            --linebreak--,
                responsive_visibility,
        ',
    ];
    $GLOBALS['TCA'][$table]['palettes']['vitd_layout_noanimation'] = [
        'showitem' => '
                boxedcontent, boxedcontent,
            --linebreak--,
                paddingtop, paddingbottom,
            --linebreak--,
                hintergrundfarbe, hintergrundbild, hintergrundmuster, hintergrundstil,
            --linebreak--,
                parallax,
            --linebreak--,
                responsive_visibility,
        ',
    ];

})('tt_content');
